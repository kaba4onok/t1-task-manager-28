package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.IRepository;
import ru.t1.rleonov.tm.enumerated.Sort;
import java.util.List;

public interface IService<M> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable Sort sort);

}
